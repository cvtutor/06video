// CVTutor.Video.Background02.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/video.hpp>
#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>

using namespace cv;
using namespace std;


// Forward decl.
int processVideo(int camIndex);
int processVideo(string videoFile);
int processVideo(VideoCapture capture);
int processImages(const vector<string> filenames);
int processImage(Mat frame, Mat& bgFrame);
int processImage(const string filename, Mat& bgFrame);


// Usage example:
// .\CVTutor.Video.Background02.exe
// .\CVTutor.Video.Background02.exe <camera index>
// .\CVTutor.Video.Background02.exe <video file>
// .\CVTutor.Video.Background02.exe ..\..\sample-frames\birds\bird0.png  ..\..\sample-frames\birds\bird1.png  ..\..\sample-frames\birds\bird2.png  ..\..\sample-frames\birds\bird3.png
// In this demo, we take the diff of two consecutive images/frames.
int main(int argc, char* argv[])
{
    auto camIndex = -1;
    auto videoFile = string();
    auto filenames = vector<string>();
    if (argc == 1) {
        camIndex = 0;
    }
    else if (argc == 2) {
        try {
            camIndex = stoi(argv[1]);
        }
        catch (const std::exception& ex) {
            // ignore
            videoFile = argv[1];
        }
    }
    else {
        for (auto i = 1; i < argc; i++) {
            filenames.push_back(argv[i]);
        }
    }

    if (camIndex >= 0) {
        processVideo(camIndex);
    }
    else if (!videoFile.empty()) {
        processVideo(videoFile);
    }
    else if (!filenames.empty()) {
        processImages(filenames);
    }
    else {
        cerr << "Incorrect input list" << endl;
        cerr << "exiting..." << endl;
        system("pause");
        return -1;
    }

    return 0;
}


int processVideo(int camIndex)
{
    VideoCapture capture{ camIndex };
    return processVideo(capture);
}
int processVideo(string videoFile)
{
    VideoCapture capture{ videoFile };
    return processVideo(capture);
}
int processVideo(VideoCapture capture)
{
    // create GUI windows
    namedWindow("Frame");
    namedWindow("Foreground segmented");

    // create Background Subtractor objects
    Ptr<BackgroundSubtractor> pMOG2 = createBackgroundSubtractorMOG2();

    Mat frame;
    capture >> frame;
    if (frame.empty()) {
        cerr << "Failed to capture the first video frame..." << endl;
        system("pause");
        return -1;
    }

    // ???
    Mat bgFrame;
    frame.copyTo(bgFrame);
    cvtColor(bgFrame, bgFrame, CV_BGR2GRAY);
    while (true) {
        capture >> frame;
        if (frame.empty()) {
            cerr << "Failed to capture video frames..." << endl;
            system("pause");
            break;
        }
        Mat fgFrame;
        cvtColor(frame, fgFrame, CV_BGR2GRAY);
        if (processImage(fgFrame, bgFrame)) {
            cerr << "Unable to process frame." << endl;
            break;
        }
        // tbd: set the fps... ?
        char keyboard = waitKey(10);
        if (keyboard == 'q' || keyboard == 27) {
            break;
        }
    }
    cout << "Processed all video frames." << endl;

    // destroy GUI windows
    destroyAllWindows();

    return 0;
}

int processImages(const vector<string> filenames)
{
    // create GUI windows
    namedWindow("Frame");
    namedWindow("Foreground segmented");

    // Background image:
    Mat bgFrame = imread(filenames[0]);
    if (bgFrame.empty()) {
        //error in opening the image
        return -1;
    }
    cvtColor(bgFrame, bgFrame, CV_BGR2GRAY);

    for (auto i = 1; i < filenames.size(); i++) {   // Note the starting index.
        auto f = filenames[i];
        if (processImage(f, bgFrame)) {
            cerr << "Unable to open image file: " << f << endl;
            exit(EXIT_FAILURE);
        }
        // tbd: set the fps... ?
        char keyboard = waitKey(10);
        if (keyboard == 'q' || keyboard == 27) {
            break;
        }
    }
    cout << "Processed all image files." << endl;

    // destroy GUI windows
    destroyAllWindows();

    return 0;
}

// Non-zero return value means error occurred.
int processImage(const string filename, Mat& bgFrame)
{
    // Read the image file.
    Mat frame = imread(filename);
    if (frame.empty()) {
        //error in opening the image
        return -1;
    }
    Mat fgFrame;
    cvtColor(frame, fgFrame, CV_BGR2GRAY);
    return processImage(fgFrame, bgFrame);
}
int processImage(Mat frame, Mat& bgFrame)
{
    static int counter = 0;

    // Just use diff...
    Mat bgSubtracted;
    absdiff(frame, bgFrame, bgSubtracted);

    rectangle(frame, cv::Point(10, 2), cv::Point(100, 20),
        cv::Scalar(255, 255, 255), -1);
    putText(frame, std::to_string(counter), cv::Point(15, 15),
        FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));

    //show the current frame and the fg masks
    imshow("Frame", frame);
    imshow("Foreground segmented", bgSubtracted);

    // replace the bgFrame with the current frame...
    // This should be really done outside this function, e.g., by the caller.
    // but, after all, this is just a demo app.
    frame.copyTo(bgFrame);

    ++counter;
    return 0;
}
