# 6. Video
Tutorials from Video module.

The repo contains examples from OpenCV 3.x tutorial docs, specifically related to dynamic video analysis
(as opposed to processing frames from video, live or prerecorded, or still images).


    git clone https://github.com/cvtutor/video.git 06video


## OpenCV Doc

* [Video analysis (video module)](http://docs.opencv.org/3.1.0/da/dd0/tutorial_table_of_content_video.html)



### CVTutor.Video.Background01

* [How to Use Background Subtraction Methods](http://docs.opencv.org/3.1.0/d1/dc5/tutorial_background_subtraction.html)

Usage:    
    `CVTutor.Video.Background01 <camera index = 0>`    
    `CVTutor.Video.Background01 <video file>`    
    `CVTutor.Video.Background01 <image file1> <image file2> ...`


### CVTutor.Video.Background02

* [Operations on Arrays](http://docs.opencv.org/2.4/modules/core/doc/operations_on_arrays.html#absdiff)

Usage:    
    `CVTutor.Video.Background02 <camera index = 0>`    
    `CVTutor.Video.Background02 <video file>`    
    `CVTutor.Video.Background02 <image file1> <image file2> <image file3>...`


### CVTutor.Video.Motion01

* Motion detection and movement tracking.

Based on background subtraction.

_TBD_


Usage:    
    `CVTutor.Video.Motion01 <camera index = 0>`    
    `CVTutor.Video.Motion01 <video file>`    
    `CVTutor.Video.Motion01 <image file1> <image file2> ...`


### CVTutor.Video.Motion02

* Motion detection and movement tracking.

Based on image diff.

_TBD_


Usage:    
    `CVTutor.Video.Motion02`


### CVTutor.Video.Track01

* Feature detection and object/feature tracking.

Demonstrates various OpenCV Tracker APIs.

_TBD_


Usage:    
    `CVTutor.Video.Track01`


